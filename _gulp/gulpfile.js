// to build the project, use:
// gulp watch --url {project_name} --min
// gulp imagemin --url {project_name} --min

// *** gem install compass
// *** compass install compass --sass-dir "sass" --javascripts-dir "js" --images-dir "images"

// delete node_modules
// *** npm install rimraf -g
// *** rimraf node_modules

let gulp = require('gulp'),
argv = require('yargs').argv,
gulpif = require('gulp-if'),
compass = require('gulp-compass'),
path = require('path'),
sass = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer');
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
cleanCSS = require('gulp-clean-css');
imagemin = require('gulp-imagemin'),
pngquant = require('imagemin-pngquant'),
uncss = require('gulp-uncss'),
rename = require('gulp-rename'),
watch = require('gulp-watch');

var url = '../' + argv.url + '/';

// task  default
gulp.task('default', function() {
  gulp.src(url + 'sass/*.scss')
    .pipe(compass({
        project: path.join(__dirname, url),
        css: 'app/unminify/',
        sass: 'sass',
        image: 'images'
    }))
    .on('error', function(error) {
        console.log('gulpError -- ' + error);
        this.emit('end');
      })
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(concat('all.css'))
    .pipe(gulp.dest(url + 'app/unminify'))
    .pipe(gulpif(argv.min, cleanCSS()))
    .pipe(gulpif(argv.min, rename({
        suffix: '.min'
    })))
    .pipe(gulpif(argv.min, gulp.dest(url + 'app/')));

  gulp.src(url + './js/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest(url + 'app/unminify'))
    .pipe(gulpif(argv.min, uglify()))
    .pipe(gulpif(argv.min, rename({suffix: '.min'})))
    .pipe(gulpif(argv.min, gulp.dest(url + 'app')));

});

// task imagemin
gulp.task('imagemin', function() {
  return gulp.src(url + './images/**/*')
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{
            removeViewBox: false
        }],
        use: [pngquant()]
    }))
    .pipe(gulp.dest(url + './images'));
});

// task watch
gulp.task('watch', function() {
  gulp.watch(url + './sass/*.scss', ['default'])
  gulp.watch(url + './js/*.js', ['default'])
  return gulp;
});
